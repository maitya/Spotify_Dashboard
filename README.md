# Spotify Dashboard

## Requires Python 3
## Install requirements
``` pip install -r requirements.txt```

## Run server
``` python server.py ```

## Browse to ``` localhost:8000 ```