var formatDate = d3.time.format("%Y-%m-%d");

/**
 * Calculates the minimum and maximum key values in data
 * @param data the data which has to be searched
 * @param key the key whose value is to be searched
 * @returns {*[]} [min, max] value of key in data
 */
function getMinMax(data, key) {

    var min = 1000000000;
    var max = -1;
    data.forEach(function (d) {

        if (d[key] > max) max = d[key];
        if (d[key] < min) min = d[key];
    });
    return [min, max];
}

/**
 * Map dataValue from [range1Min, range1Max] range to [range2Min, range2Max] range.
 * @param dataValue data value which has to be mapped
 * @param range2Min min value of target range
 * @param range2Max max value of target range
 * @param range1Min min value of source range
 * @param range1Max max value of source range
 * @param mode forward or reverse mapping
 * @returns {number} mapped data value
 */
function mapData(dataValue, range2Min, range2Max, range1Min, range1Max, mode) {

    var map = (range2Max - range2Min) * (dataValue - range1Min) / (range1Max - range1Min);
    if (mode == "reverse")
        return range2Max - map;
    else if (mode == "forward")
        return range2Min + map;
}

/**
 * Used to sort json object by date
 * @param x first json object
 * @param y second json object
 * @returns {number} difference between their date components
 */
function sortByDate(x, y) {
    var x = new Date(x.Date);
    var y = new Date(y.Date);
    return x > y ? 1 : -1;
}

/**
 * Group and sort position by date
 * @param data the data to group and sort
 * @param value the value which has to be averaged
 * @param tracks the track names to filter with
 * @returns {IterableIterator<[string , string]> | IterableIterator<[number , Node]> | IterableIterator<[number , TNode]> | IterableIterator<[string , (string | File)]>} the grouped data
 */
function groupAndSortValueByDate(data, value, tracks) {

    var groupedDataByD = d3.nest()
        .key(function (d) {
            return d.Date;
        })
        .rollup(function (d) {
            return d3.mean(d, function (g) {
                return g[value];
            });
        })
        .entries(data.filter(function (d) {
            return tracks.indexOf(d["Track Name"]) > -1;
        }));

    // Rename column names for grouped data
    groupedDataByD.forEach(function (d) {
        d.Date = d.key;
        d[value] = d.values;
    });

    groupedDataByD.sort(sortByDate);
    return groupedDataByD;
}

/**
 * Returns true if the date falls in a leap year
 * @param date the date to check
 * @returns {boolean} true if falls in leap year, else false
 */
function isLeapYear(date) {

    var year = date.getFullYear();
    if ((year & 3) != 0) return false;
    return ((year % 100) != 0 || (year % 400) == 0);
}

/**
 * Get day of year from date
 * @param date the date to get day from
 * @returns {number} the day in the year
 */
function getDayOfYear(date) {

    var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    var month = date.getMonth();
    var day = date.getDate();
    var dayOfYear = dayCount[month] + day;
    if (month > 1 && isLeapYear(date)) dayOfYear++;
    return dayOfYear;
}

/**
 * Convert CSV data to JSON required for path chart
 * @param data the data to convert
 * @param tracks the tracks to include
 * @param overallMinPos the total Min Position for the 4 tracks
 * @param overallMaxPos the total Max position for the 4 tracks
 * @param overallMinStreams the total Min Streams for the 4 tracks
 * @param overallMaxStreams the total Max Streams for the 4 tracks
 * @param dimensions the width and height array
 * @returns {Array} the array of JSON values
 */
function createPathDataset(data, tracks,
                           overallMinPos, overallMaxPos,
                           overallMinStreams, overallMaxStreams,
                           overallMinDate, overallMaxDate,
                           dimensions) {

    var points = [];
    data.forEach(function (d) {

        if (tracks.indexOf(d["Track Name"]) > -1) {
            var x = getDayOfYear(new Date(d.Date));
            var y = d.Position;
            var oneDateData = data.filter(function (g) {
                return g.Date == d.Date && tracks.indexOf(g["Track Name"]) > -1;
            });

            x = mapData(x, 50, dimensions[0], overallMinDate, overallMaxDate, "forward");
            y = mapData(y, 0, dimensions[1], overallMinPos, overallMaxPos, "forward");
            var w = mapData(oneDateData[0].Streams, 0.5, 100, overallMinStreams, overallMaxStreams, "forward");
            points.push({
                "x": x,
                "y": y,
                "w": w
            });
        }
    });

    return points;
}

/**
 * Get min and max x and y coordinates in data
 * @param data the data to convert
 * @param tracks the tracks to include
 * @param dimensions the width and height array
 * @returns {Array} the array of [minX, maxX, minY, maxY]
 */
function getMinMaxXYForPath(data, tracks, dimensions) {

    // Group and sort position by dates
    var groupedPositionByD = groupAndSortValueByDate(data, "Position", tracks);

    // Calculate max and min positions of artist's tracks
    var artistMinMaxPosition = getMinMax(groupedPositionByD, "Position");
    var artistMinPosition = artistMinMaxPosition[0];
    var artistMaxPosition = artistMinMaxPosition[1];

    var minX = 1000000000.0;
    var maxX = -1.0;
    var minY = 1000000000.0;
    var maxY = -1.0;
    groupedPositionByD.forEach(function (d) {

        var x = getDayOfYear(new Date(d.Date));
        var y = d.Position;
        x = mapData(x, 50, dimensions[0], 6, 365, "forward");
        y = mapData(y, 0, dimensions[1], artistMinPosition, artistMaxPosition, "forward");
        if (x < minX) minX = x;
        else if (x > maxX) maxX = x;
        if (y < minY) minY = y;
        else if (y > maxY) maxY = y;
    });

    return [minX, maxX, minY, maxY];
}

/**
 * Get the points in data where month changes
 * @param data the data to convert
 * @param tracks the tracks to include
 * @param dimensions the width and height array
 * @returns {Array} the array of month-change points in x
 */
function getMonthPointsForPath(data, tracks,
                               overallMinDate, overallMaxDate,
                               dimensions) {

    // Group and sort position by dates
    // var groupedPositionByD = groupAndSortValueByDate(data, "Position", tracks);
    var monthStarted = [];
    var next = 0;
    data.forEach(function (d) {

        if (tracks.indexOf(d["Track Name"]) > -1) {

            if (new Date(d.Date).getMonth() == next) {

                next++;
                var x = getDayOfYear(new Date(d.Date));
                x = mapData(x, 50, dimensions[0], getDayOfYear(overallMinDate), getDayOfYear(overallMaxDate), "forward");
                monthStarted.push(x);
            }
        }
    });
    return monthStarted;
}

/**
 * Get minimum and maximum positions of artists in data for the tracks
 * @param data the data to search in
 * @param tracks the tracks to filter with
 * @returns {*[]} array of two elements - [min position, max position]
 */
function getMinMaxArtistPositions(data, tracks) {

    var minPosition = 100000;
    var maxPosition = -1;
    data.forEach(function (d) {

        if (tracks.indexOf(d["Track Name"]) > -1) {

            if (d.Position < minPosition) minPosition = d.Position;
            else if (d.Position > maxPosition) maxPosition = d.Position;
        }
    });
    return [minPosition, maxPosition];
}

/**
 * Get minimum and maximum streams of artists in data for the tracks
 * @param data the data to search in
 * @param tracks the tracks to filter with
 * @returns {*[]} array of two elements - [min streams, max streams]
 */
function getMinMaxArtistStreams(data, tracks) {

    var minStreams = 1000000000;
    var maxStreams = -1;
    data.forEach(function (d) {

        if (tracks.indexOf(d["Track Name"]) > -1) {

            if (d.Streams < minStreams) minStreams = d.Streams;
            else if (d.Streams > maxStreams) maxStreams = d.Streams;
        }
    });
    return [minStreams, maxStreams];
}

/**
 * Get minimum and maximum dates of artists in data for the tracks
 * @param data the data to search in
 * @param tracks the tracks to filter with
 * @returns {*[]} array of two elements - [min date, max date]
 */
function getMinMaxArtistDates(data, tracks) {

    var minDate = formatDate.parse("2020-01-01");
    var maxDate = formatDate.parse("1999-01-01");
    data.forEach(function (d) {

        if (tracks.indexOf(d["Track Name"]) > -1) {

            var parsedDate = formatDate.parse(d.Date);
            if (parsedDate < minDate) minDate = parsedDate;
            else if (parsedDate > maxDate) maxDate = parsedDate;
        }
    });
    return [minDate, maxDate];
}

/**
 * Get the minimum date value in the data for the track
 * @param data data to search in
 * @param track track to filter with
 * @returns {number | any} minimum date value
 */
function getMinDate(data, track) {

    var minDate = formatDate.parse("2020-01-01");
    data.forEach(function (d) {

        if (d["Track Name"] == track) {

            var parsedDate = formatDate.parse(d.Date);
            if (parsedDate < minDate) minDate = parsedDate;
        }
    });
    return minDate;
}

/**
 * Get the values each day in the data for the track
 * @param data data to search in
 * @param track track to filter with
 * @param field the field to return data each day
 * @returns {Array} list of positions during the year
 */
function getValuesPerDay(data, track, field) {

    var positions = []
    data.forEach(function (d) {

        if (d["Track Name"] == track) {

            positions.push(d[field]);
        }
    });
    return positions;
}

/**
 * Get color for a particular track to draw lines
 * @param track
 * @returns {string} the color for the corresponding track
 */
function getColor(track) {

    if (track == "Shape of You") return "#ff8000";
    else if (track == "Castle on the Hill") return "#FF9999";
    else if (track == "Galway Girl") return "#7CFC00";
    else if (track == "Perfect") return "#00bfff";
}

/**
 * Get color for a particular track to draw blocks
 * @param track
 * @returns {string} the color for the corresponding track
 */
function getLightColor(track) {

    if (track == "Shape of You") return "#ff9933";
    else if (track == "Castle on the Hill") return "#FF3333";
    else if (track == "Galway Girl") return "#b2ff66";
    else if (track == "Perfect") return "#33ffff";
}

// Month names
const MONTH_NAMES = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

// Runs when the body of HTML loads
function init(data) {

    var pathTip; // tool tip variable for path chart
    var pieTip; // tool tip variable for pie chart
    var trackLoops = []; // stores the ordering of track while looping to create blockly line chart
    var minDateOfTracks = []; // stores minimum date of each track
    var datesTrackWise = []; // stores date each day track wise
    var positionsTrackWise = []; // stores positions each day track wise
    var streamsTrackWise = []; // stores streams each day track wise

    // add circle axes to pie
    function addCircleAxes(svg, sdat, sdatMapped) {

        var circleAxes, i;
        circleAxes = svg.selectAll('.circle-ticks')
            .data(sdatMapped)
            .enter().append('svg:g')
            .attr("class", "circle-ticks");

        circleAxes.append("svg:circle")
            .attr("r", String)
            .attr("class", "circle")
            .style("stroke", "#CCC")
            .style("opacity", 0.5)
            .style("fill", "none");

        circleAxes.append("svg:text")
            .attr("text-anchor", "center")
            .attr("dy", function (d) {
                return d + 10
            })
            .style("fill", "white")
            .style("font-weight", "bold")
            .style("font-size", "12px")
            .text(function (d, i) {
                return parseFloat(sdat[i]).toFixed(1);
            });
    }

    // Defines what will happen if user hovers over blockly line
    function handleBlocklyLineOver(d, i) {

        // Get Track Name
        var classVal = d3.select(this).attr("class");
        var trackIdx = classVal.split("-")[1];
        var trackName = trackLoops[trackIdx];

        var date = datesTrackWise[trackIdx][i - 1];
        var position = positionsTrackWise[trackIdx][i - 1];
        var streams = streamsTrackWise[trackIdx][i - 1];

        var tipData = {
            "Track Name": trackName,
            "Date": new Date(date),
            "Position": position,
            "Streams": streams
        };
        d3.select(this)
            .transition()
            .style("opacity", 1);
        pathTip.show(tipData);
    }

    // Defines what will happen if user removes hovering over blockly line
    function handleBlocklyLineOut() {

        d3.select(this)
            .transition()
            .duration(500)
            .style("opacity", 0.6);
        pathTip.hide();
    }

    // Render time-series pie chart
    function renderPie(data, tracks) {

        // Group and sort position by dates
        var groupedDataByD = groupAndSortValueByDate(data, "Position", tracks);

        // Calculate length of data
        var dataLen = groupedDataByD.length;

        // Set pie's parameters
        var pieMinLength = 50;
        var pieMaxLength = 275;
        var pieInnerRadius = 20;

        // Calculate max and min positions of artist's tracks
        var artistMinMaxPosition = getMinMax(groupedDataByD, "Position");
        var artistMinPosition = artistMinMaxPosition[0];
        var artistMaxPosition = artistMinMaxPosition[1];

        // Set pie's SVG parameters
        var width = 600,
            height = 600,
            radius = Math.min(width, height) / 2 - 10;

        var monthStart = new Array(12); // Stores when the new month starts in data

        // Create arc in pie
        var arc = d3.svg.arc()
            .outerRadius(function (d, i) {

                var date = new Date(d.data.Date);
                var month = date.getMonth();
                if (monthStart[month] == undefined) monthStart[month] = i;

                var outRad = mapData(d.data.Position, pieMinLength, pieMaxLength, artistMinPosition, artistMaxPosition, "reverse");
                return outRad;
            })
            .innerRadius(pieInnerRadius);

        // Create pie
        var pie = d3.layout.pie()
            .sort(null)
            .value(function (d) {
                return 1;
            });

        // Create SVG element
        var svg = d3.select(".pie-chart")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        // Define and set up the tooltip
        pieTip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function (d) {

                var date = new Date(d.data.Date);
                return "Date: " + date.getDate() + "-" + MONTH_NAMES[date.getMonth()] + "-" + date.getFullYear() + "<br/>" +
                    "Avg. Ranking: " + d.data.Position;
            });
        svg.call(pieTip);

        // Create an arc pie chart
        var arcPie = svg.selectAll(".arc")
            .data(pie(groupedDataByD))
            .enter().append("g")
            .attr("class", "arc")
            .append("path")
            .attr("d", arc)
            .on("mouseover", pieTip.show)
            .on("mouseout", pieTip.hide)
            .style("fill", function (d) {
                return "#7CFC00"; // all green color pies
            });

        // Prep for radial lines
        var radialPoints = [];
        for (var i = 0; i < monthStart.length; i++) {

            if (monthStart[i] != undefined) {

                var angle = (monthStart[i] * 2 * Math.PI / dataLen) - Math.PI / 2;
                var x2 = radius * Math.cos(angle);
                var y2 = radius * Math.sin(angle);
                radialPoints.push([x2, y2]);
            }
        }

        // Create radial lines
        var radLines = svg.selectAll(".radial-line")
            .data(radialPoints)
            .enter().append("line")
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", function (d) {
                return d[0];
            })
            .attr("y2", function (d) {
                return d[1];
            })
            .attr("class", "radial-line")
            .attr("stroke", "gray")
            .attr("text", "ABCD");

        // Prep for text near radial lines
        var radialPointsText = [];
        var j = 0;
        for (var i = 0; i < monthStart.length; i++) {

            if (monthStart[i] != undefined) {

                radialPointsText.push([radialPoints[j][0], radialPoints[j][1], MONTH_NAMES[i]]);
                j++;
            }
        }

        // Create text near radial lines
        var radText = svg.selectAll(".radial-line-text")
            .data(radialPointsText)
            .enter().append("text")
            .attr("x", function (d) {
                return d[0] + 5;
            })
            .attr("y", function (d) {
                return d[1] + 5;
            })
            .attr("class", "radial-line-text")
            .attr('text-anchor', 'middle')
            .text(function (d) {
                return d[2];
            })
            .style("fill", "white");

        // Prep for adding circle axes
        var spacing = 5; // number of circle axes
        var diff = artistMaxPosition - artistMinPosition;
        if (diff < 5) spacing = diff;
        var quotient = diff / spacing;
        var remainder = diff % spacing;
        var sdat = new Array(spacing + 1); // stores where the circle axes should be
        var sdatMapped = new Array(spacing + 1); // map the sdat data to new dimension
        var previous = 0;
        for (var i = 1; i <= spacing; i++) {

            if (i > 1) previous = sdat[i - 1];
            sdat[i] = previous + (i < remainder ? quotient + 1 : quotient);
        }
        sdat[0] = 0;
        for (var i = 0; i <= spacing; i++) sdat[i] += artistMinPosition;
        for (var i = 0; i <= spacing; i++)
            sdatMapped[i] = mapData(sdat[i],
                pieMinLength, pieMaxLength, artistMinPosition, artistMaxPosition, "reverse");

        addCircleAxes(svg, sdat, sdatMapped);
    }

    // Render blockly line chart
    function renderPath(data, tracks) {

        var container = document.getElementById("path-chart-container");
        var container_width = container.offsetWidth - 40;
        var container_height = 600;

        var svg = d3.select(".path-chart").append("svg")
            .attr("width", container_width)
            .attr("height", container_height)
            .style("padding-top", "75px")
            .style("padding-bottom", "50px")
            .attr("class", "path-chart-svg");
        var trail = d3.layout.trail().coordType("xy");

        // Define and set up the tooltip
        pathTip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function (d) {

                return "Track: " + d["Track Name"] + "<br/>" +
                    "Date: " + d.Date.getDate() + "-" + MONTH_NAMES[d.Date.getMonth()] + "-" + d.Date.getFullYear() + "<br/>" +
                    "Ranking: " + d.Position + "<br/>" +
                    "Streams: " + d.Streams;
            });
        svg.call(pathTip);

        var minMaxXY = getMinMaxXYForPath(data, tracks, [container_width, container_height]);
        var minX = minMaxXY[0];
        var maxX = minMaxXY[1];
        var minY = minMaxXY[2];
        var maxY = minMaxXY[3];

        var minMaxArtistDates = getMinMaxArtistDates(data, tracks);
        var minDate = minMaxArtistDates[0];
        var maxDate = minMaxArtistDates[1];

        var monthStarted = getMonthPointsForPath(data, tracks,
            minDate, maxDate,
            [container_width, container_height]);
        var newMonthStarted = monthStarted.slice(1);

        // Draw legend box
        document.getElementsByClassName("at-front-text")[0].style.left = monthStarted[0] + 50 + "px";
        document.getElementsByClassName("select-tracks-radio")[0].style.left = monthStarted[0] + 50 + "px";

        // Draw intermediate y-axis
        svg.selectAll("line.int-yAxis")
            .data(newMonthStarted)
            .enter()
            .append("line")
            .attr("x1", function (d) {
                return d;
            })
            .attr("y1", minY - 50)
            .attr("x2", function (d) {
                return d;
            })
            .attr("y2", maxY + 10)
            .attr("stroke", "white")
            .attr("stroke-width", 1)
            .attr("stroke-dasharray", ("3, 3"))
            .attr("opacity", 0.5)
            .attr("class", "int-yAxis");

        // Prep for intermediate x-axis
        var minMaxArtistPositions = getMinMaxArtistPositions(data, tracks);
        var minPosition = minMaxArtistPositions[0];
        var maxPosition = minMaxArtistPositions[1];

        // Divide min and max artist positions into five components
        var numPartitions = 5;
        var diff = maxPosition - minPosition;
        var partitions = [];
        if (diff + 1 > numPartitions) {

            var partitionDiff = diff / (numPartitions - 1);
            for (var i = 0; i < numPartitions; i++)
                partitions.push(Math.round(minPosition + i * partitionDiff));
        }
        else {

            var i = 0;
            while (diff >= 0) {

                partitions.push(Math.round(minPosition + i));
                i++;
                diff--;
            }
        }

        // Convert artist positions to pixel data
        var mappedPartitions = [];
        for (var i = 0; i < partitions.length; i++)
            mappedPartitions.push(mapData(partitions[i], 0, container_height, minPosition, maxPosition, "forward"));

        // Draw intermediate x-axis
        svg.selectAll("line.int-xAxis")
            .data(mappedPartitions)
            .enter()
            .append("line")
            .attr("x1", minX - 10)
            .attr("y1", function (d) {
                return d;
            })
            .attr("x2", maxX)
            .attr("y2", function (d) {
                return d;
            })
            .attr("stroke", "white")
            .attr("stroke-width", 1)
            .attr("stroke-dasharray", ("3, 3"))
            .attr("opacity", 0.5)
            .attr("class", "int-yAxis");

        // Draw x-axis
        svg.append("line")
            .attr("x1", minX - 5)
            .attr("y1", maxY)
            .attr("x2", maxX)
            .attr("y2", maxY)
            .attr("stroke", "white")
            .attr("stroke-width", 1)
            .attr("opacity", 0.5)
            .attr("class", "xAxis");

        // x-axis label
        svg.selectAll("text.xAxisLabel")
            .data(monthStarted)
            .enter()
            .append("text")
            .attr("x", function (d) {
                return d - 10;
            })
            .attr("y", maxY + 20)
            .text(function (d, i) {
                return MONTH_NAMES[i];
            })
            .style("fill", "white")
            .attr("class", "xAxisLabel");

        // Draw y-axis
        svg.append("line")
            .attr("x1", minX)
            .attr("y1", minY - 50)
            .attr("x2", minX)
            .attr("y2", maxY + 10)
            .attr("stroke", "white")
            .attr("stroke-width", 1)
            .attr("opacity", 0.5)
            .attr("class", "yAxis");

        // y-axis label
        svg.selectAll("text.yAxisLabel")
            .data(mappedPartitions)
            .enter()
            .append("text")
            .attr("x", minX - 25)
            .attr("y", function (d) {
                return d + 2.5;
            })
            .text(function (d, i) {
                return partitions[i];
            })
            .style("fill", "white")
            .attr("class", "yAxisLabel");

        var minMaxArtistStreams = getMinMaxArtistStreams(data, tracks);
        var minStreams = minMaxArtistStreams[0];
        var maxStreams = minMaxArtistStreams[1];

        function atTop(tracks) {

            trackLoops = [];
            minDateOfTracks = [];
            datesTrackWise = [];
            positionsTrackWise = [];
            streamsTrackWise = [];

            for (var i = 0; i < tracks.length; i++) {

                trackLoops.push(tracks[i]);
                minDateOfTracks.push(getMinDate(data, tracks[i]));
                datesTrackWise.push(getValuesPerDay(data, tracks[i], "Date"));
                positionsTrackWise.push(getValuesPerDay(data, tracks[i], "Position"));
                streamsTrackWise.push(getValuesPerDay(data, tracks[i], "Streams"));

                var points = createPathDataset(data, tracks[i],
                    minPosition, maxPosition,
                    minStreams, maxStreams,
                    getDayOfYear(minDate), getDayOfYear(maxDate),
                    [container_width, container_height]);

                var trail_layout = trail.data(points).layout();
                var paths = svg.selectAll("line.line-" + i).data(trail_layout);

                // blockly line chart
                paths.enter()
                    .append("line")
                    .style("stroke-width", function (d) {
                        return d.w;
                    })
                    .style("stroke", getColor(tracks[i]))
                    .style("opacity", 0.6)
                    .attr("x1", function (d) {
                        var x = d.x1;
                        if (x < minX) minX = x;
                        else if (x > maxX) maxX = x;
                        return x;
                    })
                    .attr("y1", function (d) {
                        var y = d.y1;
                        if (y < minY) minY = y;
                        else if (y > maxY) maxY = y;
                        return y;
                    })
                    .attr("x2", function (d) {
                        var x = d.x2;
                        if (x < minX) minX = x;
                        else if (x > maxX) maxX = x;
                        return x;
                    })
                    .attr("y2", function (d) {
                        var y = d.y2;
                        if (y < minY) minY = y;
                        else if (y > maxY) maxY = y;
                        return y;
                    })
                    .on("mouseover", handleBlocklyLineOver)
                    .on("mouseout", handleBlocklyLineOut)
                    .attr("class", "line-" + i);

                // line chart within the blocks
                paths.enter()
                    .append("line")
                    .style("stroke-width", 1)
                    .style("stroke", getLightColor(tracks[i]))
                    .style("opacity", 1)
                    .attr("x1", function (d) {
                        return d.x1;
                    })
                    .attr("y1", function (d) {
                        return d.y1;
                    })
                    .attr("x2", function (d) {
                        return d.x2;
                    })
                    .attr("y2", function (d) {
                        return d.y2;
                    })
                    .on("mouseover", handleBlocklyLineOver)
                    .on("mouseout", handleBlocklyLineOut)
                    .attr("class", "line-" + i);
            }
        }

        atTop(tracks);
    }

    // Group by track names
    var groupedDataByTN = d3.nest()
        .key(function (d) {
            return d["Track Name"];
        })
        .rollup(function (d) {
            return null;
        })
        .entries(data);

    // Get distinct track names and assign to check box
    var distinctTracks = []
    groupedDataByTN.forEach(function (d, i) {

        distinctTracks.push(d.key);
        document.getElementsByClassName("select-tracks")[0].innerHTML +=
            "<input type=\"checkbox\" id=\"track-" + i + "\" value=\"" + i + "\" checked>" +
            "<span class=\"tracks\">" + d.key + "</span>" +
            "<br/>";
    });

    // Filter the visualization by track
    function filterTrack() {

        d3.selectAll("svg.pie-chart > *").remove();
        var tracksToInclude = [];
        for (var i = 0; i < distinctTracks.length; i++) {
            if (document.getElementById("track-" + i).checked)
                tracksToInclude.push(distinctTracks[i]);
        }
        renderPie(data, tracksToInclude);
    }

    function updatePath() {

        d3.selectAll("svg.path-chart-svg").remove();
        var selectedTrack;
        for (var i = 0; i < distinctTracks.length; i++) {

            if (document.getElementById("track-radio-" + i).checked) {

                selectedTrack = distinctTracks[distinctTracks.length - i - 1];
                break;
            }
        }
        var trackOrder = [];
        trackOrder.push(distinctTracks[distinctTracks.length - i - 1]);
        for (var i = 0; i < distinctTracks.length; i++) {
            if (distinctTracks[distinctTracks.length - i - 1] != selectedTrack)
                trackOrder.push(distinctTracks[distinctTracks.length - i - 1]);
        }
        trackOrder.reverse();
        renderPath(data, trackOrder);

        // Add click event listener to radio boxes
        for (var i = 0; i < distinctTracks.length; i++)
            document.getElementById("track-radio-" + i).addEventListener("click", updatePath);
    }

    // Add click event listener to check boxes
    for (var i = 0; i < distinctTracks.length; i++)
        document.getElementById("track-" + i).addEventListener("click", filterTrack);

    renderPie(data, distinctTracks);
    renderPath(data, distinctTracks);

    distinctTracks.reverse();
    // Fill radio button with tracks
    document.getElementsByClassName("select-tracks-radio")[0].innerHTML = "";
    for (var i = 0; i < distinctTracks.length; i++) {

        document.getElementsByClassName("select-tracks-radio")[0].innerHTML +=
            "<input type=\"radio\" name=\"tracks\" id=\"track-radio-" + i + "\" value=\"" + i + "\">" +
            "<span class=\"tracks\">" + distinctTracks[i] + "</span>" +
            "<br/>";
    }
    document.getElementById("track-radio-0").checked = true;
    distinctTracks.reverse();

    // Add click event listener to radio boxes
    for (var i = 0; i < distinctTracks.length; i++)
        document.getElementById("track-radio-" + i).addEventListener("click", updatePath);
}