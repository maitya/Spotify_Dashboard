from flask import Flask, render_template
import pandas as pd
import time

app = Flask(__name__)

def load_csv(file):
    """Loads CSV and returns a dataframe."""
    df = pd.read_csv(file)
    return df

def filter(df):
    """Filter data"""
    return df[(df["Artist"] == "Ed Sheeran") &
              ((df["Track Name"] == "Shape of You") |
              (df["Track Name"] == "Perfect") |
              (df["Track Name"] == "Galway Girl") |
              (df["Track Name"] == "Castle on the Hill") |
              (df["Track Name"] == "How Would You Feel (Paean)")) &
              (df["Region"] == "global") &
              (df["Date"] >= "2017-01-01") &
              (df["Date"] <= "2017-12-31")]


@app.route("/")
def index():
    """Index Page."""
    print("Loading data...")
    start = time.time()
    df = load_csv("static/data/data_eds_soy_p_gg_coth_global.csv")
    # df = load_csv("static/data/data.csv")
    end = time.time()
    print("Loaded data in {} seconds".format((end - start)))
    # filtered_df = filter(df)
    # filtered_df.to_csv("static/data/data_eds_soy_p_gg_coth_hwyfp_global.csv")
    return render_template("index.html", df=df.to_json(orient="records"))


if __name__ == "__main__":
    """Main method."""
    app.run(port=8000)
